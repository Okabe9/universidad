ENTITY funcio IS
PORT ( a,b,c,d: IN BIT; f: OUT BIT);
END funcio;


ARCHITECTURE logica OF funcio IS
BEGIN
f <= ((((NOT a) AND b AND (NOT c)) OR (b AND (NOT d)) OR (a AND c AND d) OR  (a AND (NOT d)))AND (NOT(a OR (NOT d))))OR (  NOT(((NOT a) AND b AND (NOT c)) OR (b AND (NOT d)) OR (a AND c AND d) OR (a AND (NOT d)))AND (a OR (NOT d))) AFTER 5 ns;
END logica;

ARCHITECTURE logicaretard OF funcio IS
BEGIN
f <= ((((NOT a) AND b AND (NOT c)) OR (b AND (NOT d)) OR (a AND c AND d) OR  (a AND (NOT d)))AND (NOT(a OR (NOT d))))OR (  NOT(((NOT a) AND b AND (NOT c)) OR (b AND (NOT d)) OR (a AND c AND d) OR (a AND (NOT d)))AND(a OR (NOT d))) AFTER 3 ns;
END logicaretard;


ARCHITECTURE ifthen OF funcio IS
BEGIN

PROCESS (d, c, b, a)
BEGIN
IF d='0' AND c='0' AND b='0' AND a='0' THEN
f <='1' AFTER 5 ns;
ELSIF d='1' AND c='0' AND b='0' AND a='0' THEN
f <='0' AFTER 5 ns;
ELSIF d='0' AND c='1' AND b='0' AND a='0' THEN  
f <='1' AFTER 5 ns;
ELSIF d='1' AND c='1' AND b='0' AND a='0' THEN
f <='0' AFTER 5 ns;
ELSIF d='0' AND c='0' AND b='1' AND a='0' THEN
f <='0' AFTER 5 ns;
ELSIF d='1' AND c='0' AND b='1' AND a='0' THEN
f <='1' AFTER 5 ns;
ELSIF d='0' AND c='1' AND b='1' AND a='0' THEN
f <='0' AFTER 5 ns;
ELSIF d='1' AND c='1' AND b='1' AND a='0' THEN
f <='0' AFTER 5 ns;
ELSIF d='0' AND c='0' AND b='0' AND a='1' THEN
f <='0' AFTER 5 ns;
ELSIF d='1' AND c='0' AND b='0' AND a='1' THEN
f<='1' AFTER 5 ns;
ELSIF d='0' AND c='1' AND b='0' AND a='1' THEN  
f <='0' AFTER 5 ns;     
ELSIF d='1' AND c='1' AND b='0' AND a='1' THEN
f <='0' AFTER 5 ns;
ELSIF d='0' AND c='0' AND b='1' AND a='1' THEN
f <='0' AFTER 5 ns;
ELSIF d='1' AND c='0' AND b='1' AND a='1' THEN
f <='1' AFTER 5 ns;
ELSIF d='0' AND c='1' AND b='1' AND a='1' THEN
f <='0' AFTER 5 ns;
ELSIF d='1' AND c='1' AND b='1' AND a='1' THEN
f <='0' AFTER 5 ns;
END IF;
END PROCESS;
END ifthen;


ARCHITECTURE estructural OF funcio IS

COMPONENT portaand2 IS
PORT(a,b: IN BIT; 
	z: OUT BIT);
END COMPONENT;

COMPONENT portaor3 IS
PORT(a,b,c: IN BIT;
	z: OUT BIT);
END COMPONENT;

COMPONENT portaor2 IS
PORT(a,b: IN BIT;
	z: OUT BIT);
END COMPONENT;

COMPONENT portainv IS
PORT(a: IN BIT;
	z: OUT BIT);
END COMPONENT;

COMPONENT portaand4 IS
PORT(a,b,c,d: IN BIT;
	z:  OUT BIT);
END COMPONENT;

COMPONENT portaxor2 IS
PORT(a,b: IN BIT;
	z: OUT BIT);
END COMPONENT;
--Si fem la funci� veiem que necesitarem 2 or3(alpha y beta) 2or2(gamma y omega) 1 and4(epsilon) 1 xor2(resultat)i 3inv (inva,invc,invd)i un and2(tetta)
SIGNAL ent3, ent2, ent1, ent0, alpha, beta, gamma, omega, epsilon,tetta, resultat, inva, invc, invd: BIT;

FOR DUT1: portainv USE ENTITY WORK.inv(logicaretard);
FOR DUT2: portainv USE ENTITY WORK.inv(logicaretard);
FOR DUT3: portainv USE ENTITY WORK.inv(logicaretard);
FOR DUT4: portaor2 USE ENTITY WORK.or2(logicaretard);
FOR DUT5: portaor2 USE ENTITY WORK.or2(logicaretard);
FOR DUT6: portaor3 USE ENTITY WORK.or3(logicaretard);
FOR DUT7: portaor3 USE ENTITY WORK.or3(logicaretard);
FOR DUT8: portaand2 USE ENTITY WORK.and2(logicaretard);
FOR DUT9: portaand4 USE ENTITY WORK.and4(logicaretard);
FOR DUT10: portaxor2 USE ENTITY WORK.xor2(logicaretard);			


BEGIN
DUT1: portainv PORT MAP(ent0,inva);
DUT2: portainv PORT MAP(ent3,invd);
DUT3: portainv PORT MAP(ent2,invc);
DUT4: portaor2 PORT MAP(ent1,invd, gamma);
DUT5: portaor2 PORT MAP(ent0,invd, omega);
DUT6: portaor3 PORT MAP(inva,b,invc, alpha);
DUT7: portaor3 PORT MAP(ent0, ent2, ent3, beta);
DUT8: portaand2 PORT MAP(ent0,invd, tetta);
DUT9: portaand4 PORT MAP(alpha, gamma, beta, omega, epsilon);
DUT10: portaxor2 PORT MAP(epsilon,tetta,resultat);
END estructural;


ENTITY bdp1 IS
END bdp1;

ARCHITECTURE test OF bdp1 IS

COMPONENT portes IS
PORT (d, c, b, a: IN BIT; f: OUT BIT);
END COMPONENT;

SIGNAL ent3, ent2, ent1, ent0, sort_logica, sort_logicaretard, sort_estructural, sort_ifthen: BIT;


FOR DUT1: portes USE ENTITY WORK.funcio(ifthen);
FOR DUT2: portes USE ENTITY WORK.funcio(logica);
FOR DUT3: portes USE ENTITY WORK.funcio(estructural);
FOR DUT4: portes USE ENTITY WORK.funcio(logicaretard);

BEGIN
DUT1: portes PORT MAP(ent3, ent2, ent1, ent0, sort_ifthen);
DUT2: portes PORT MAP(ent3, ent2, ent1, ent0, sort_logica);
DUT3: portes PORT MAP(ent3, ent2, ent1, ent0, sort_estructural);
DUT4: portes PORT MAP(ent3, ent2, ent1, ent0, sort_logicaretard);
PROCESS(ent3,ent2,ent1,ent0)
BEGIN
ent3 <= NOT ent3 AFTER 300 ns;
ent2 <= NOT ent2 AFTER 200 ns;
ent1 <= NOT ent1 AFTER 100 ns;
ent0 <= NOT ent0 AFTER 50 ns;
END PROCESS;
END test;



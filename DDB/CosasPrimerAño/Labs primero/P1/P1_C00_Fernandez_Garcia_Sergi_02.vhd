--Banc de proves inversor 
ENTITY bdp IS                           
END bdp;

ARCHITECTURE test OF bdp IS
COMPONENT inversor IS
	PORT(a:IN BIT; z:OUT BIT);
END COMPONENT;

COMPONENT and2 IS
	PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT or2 IS
	PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT and3 IS
	PORT(a,b,c:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT or3 IS
	PORT(a,b,c:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT and4 IS
	PORT(a,b,c,d:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT or4 IS
	PORT(a,b,c,d:IN BIT; z: OUT BIT);
END COMPONENT; 

COMPONENT xor2 IS
	PORT(a,b: IN BIT; z: OUT BIT);
END COMPONENT;

FOR DUT1: inversor USE ENTITY WORK.inversor(logica);
FOR DUT2: and2 USE ENTITY WORK.and2(logica);
FOR DUT3: or2 USE ENTITY WORK.or2(logica);
FOR DUT4: and3 USE ENTITY WORK.and3(logica);
FOR DUT5: or3 USE ENTITY WORK.or3(logica);
FOR DUT6: and4 USE ENTITY WORK.and4(logica);
FOR DUT7: or4 USE ENTITY WORK.or4(logica); 
FOR DUT8: inversor USE ENTITY WORK.inversor(logicaretard);
FOR DUT9: and2 USE ENTITY WORK.and2(logicaretard);
FOR DUT10: or2 USE ENTITY WORK.or2(logicaretard);
FOR DUT11: and3 USE ENTITY WORK.and3(logicaretard);
FOR DUT12: or3 USE ENTITY WORK.or3(logicaretard);
FOR DUT13: and4 USE ENTITY WORK.and4(logicaretard);
FOR DUT14: or4 USE ENTITY WORK.or4(logicaretard); 
FOR DUT15: xor2 USE ENTITY WORK.xor2(logica);
FOR DUT16: xor2 USE ENTITY WORK.xor2(logicaretard);


SIGNAL a,b,c,d,sort_inv_logica, sort_and2_logica, sort_or2_logica, sort_and3_logica, sort_or3_logica, sort_and4_logica, sort_or4_logica, sort_inv_logicaretard, sort_and2_logicaretard, sort_or2_logicaretard, sort_and3_logicaretard, sort_or3_logicaretard, sort_and4_logicaretard, sort_or4_logicaretard, sort_xor2_logica, sort_xor2_logicaretard: BIT;

BEGIN

DUT1: inversor PORT MAP(a,sort_inv_logica);
DUT2: and2 PORT MAP(a,b,sort_and2_logica);
DUT3: or2 PORT MAP(a,b, sort_or2_logica);
DUT4: and3 PORT MAP(a,b,c, sort_and3_logica);
DUT5: or3 PORT MAP(a,b,c, sort_or3_logica);
DUT6: and4 PORT MAP(a,b,c,d, sort_and4_logica);
DUT7: or4 PORT MAP (a,b,c,d, sort_or4_logica);
DUT8: inversor PORT MAP(a,sort_inv_logicaretard);
DUT9: and2 PORT MAP(a,b,sort_and2_logicaretard);
DUT10: or2 PORT MAP(a,b, sort_or2_logicaretard);
DUT11: and3 PORT MAP(a,b,c, sort_and3_logicaretard);
DUT12: or3 PORT MAP(a,b,c, sort_or3_logicaretard);
DUT13: and4 PORT MAP(a,b,c,d, sort_and4_logicaretard);
DUT14: or4 PORT MAP (a,b,c,d, sort_or4_logicaretard);
DUT15: xor2 PORT MAP (a,b, sort_xor2_logica);
DUT16: xor2 PORT MAP (a,b, sort_xor2_logicaretard);
PROCESS(a,b,c,d)
BEGIN
	a<= NOT a AFTER 50 ns; 
	b<= NOT b AFTER 100 ns; 
	c<= NOT c AFTER 200 ns;
	d<= NOT d AFTER 300 ns; 
END PROCESS;
END test;

ENTITY operacio IS
PORT (a,b,c: IN BIT; z: OUT BIT);
END operacio;
ARCHITECTURE logica OF operacio IS
BEGIN
z <= (a OR b OR (not c)) AND not( (not b OR c) OR (a AND c))  ;
END logica; 


ENTITY funcio_logica IS	
PORT(sa,a1,a0: IN BIT;
 fs,f1,f0: OUT BIT);
END funcio_logica;

ARCHITECTURE logicaretard of funcio_logica IS
BEGIN
fs <= sa AFTER 3 ns;
f1 <= sa XOR a1;
f0 <= a0;
END logicaretard;

ARCHITECTURE ifthen OF funcio_logica IS
BEGIN
PROCESS(sa,a1,a0)
BEGIN
	IF sa='0' AND a1='0' AND a0='0' THEN
		fs<='0';  f1<='0'; f0<='0' AFTER 3 ns;
	
	ELSIF sa='0' AND a1='0' AND a0='1' THEN
		fs<='0';  f1<='0'; f0<='1' AFTER 3 ns;
	
	ELSIF sa='0' AND a1='1' AND a0='0' THEN
		fs<='0';  f1<='1'; f0<='0' AFTER 3 ns;
	
	ELSIF sa='0' AND a1='1' AND a0='1' THEN
		fs<='0';  f1<='1'; f0<='1' AFTER 3 ns;

	ELSIF sa='1' AND a1='0' AND a0='0' THEN
		fs<='1';  f1<='0'; f0<='0' AFTER 3 ns;
	
	ELSIF sa='1' AND a1='0' AND a0='1' THEN
		fs<='1';  f1<='1'; f0<='1' AFTER 3 ns;
	
	ELSIF sa='1' AND a1='1' AND a0='0' THEN
		fs<='1';  f1<='1'; f0<='0' AFTER 3 ns;
	
	ELSIF sa='1' AND a1='1' AND a0='1' THEN
		fs<='1';  f1<='0'; f0<='1' AFTER 3 ns;
	END IF;
END PROCESS;
END ifthen;

ARCHITECTURE estructural OF funcio_logica IS
COMPONENT portaxor2 IS
PORT(a,b: IN BIT; 	
	z: OUT BIT);
END COMPONENT;

COMPONENT portaequal IS
PORT(a: IN BIT;
	z: OUT BIT);
END COMPONENT;

SIGNAL sea,ae1,ae0: BIT;
FOR DUT1: portaxor2 USE ENTITY WORK.xor2(logicaretard);
FOR DUT2: portaequal USE ENTITY WORK.equal(logicaretard);
FOR DUT3: portaequal USE ENTITY WORK.equal(logicaretard);
BEGIN
DUT1: portaxor2 PORT MAP(sea,ae1,f1);
DUT2: portaequal PORT MAP(sea,fs);
DUT3: portaequal PORT MAP(ae0,f0);
END estructural;

ENTITY bdp2 IS
END bdp2;
ARCHITECTURE test OF bdp2 IS
COMPONENT porta_funcio
PORT(sa,a1,a0: IN BIT;
	fs,f1,f0: OUT BIT);
END COMPONENT;

SIGNAL senyala,senyalb,senyalc,sortida_sa,sortida_a1,sortida_a0: BIT;

FOR DUT1: porta_funcio USE ENTITY WORK.funcio_logica(logicaretard);
--FOR DUT2: porta_funcio USE ENTITY WORK.funcio_logica(logicaretard);
--FOR DUT3: porta_funcio USE ENTITY WORK.funcio_logica(logicaretard);

BEGIN

DUT1: porta_funcio PORT MAP(senyala,senyalb,senyalc, sortida_sa, sortida_a1, sortida_a0);
--DUT2: porta_funcio PORT MAP(senyala,senyalb,senyalc, sortida_a1);
--DUT3: porta_funcio PORT MAP(senyala,senyalb,senyalc, sortida_a0);

PROCESS (senyala,senyalb,senyalc)
	BEGIN
	senyala <= NOT senyala AFTER 50 ns;
	senyalb <= NOT senyalb AFTER 100 ns;
	senyalc <= NOT senyalc AFTER 200 ns;
END PROCESS;
END test;
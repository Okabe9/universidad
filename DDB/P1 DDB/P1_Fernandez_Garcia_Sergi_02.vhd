ENTITY bdp_portes IS
END bdp_portes;

ARCHITECTURE test OF bdp_portes IS
-- Declaraci� de components: 
COMPONENT la_porta_and2
PORT (a,b: IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT la_porta_and3
PORT(a,b,c: IN BIT; z: OUT BIT);
END COMPONENT; 

COMPONENT la_porta_and4
PORT(a,b,c,d: IN BIT; z: OUT BIT);
END COMPONENT; 

COMPONENT la_porta_or2
PORT(a,b: IN BIT; z: OUT BIT);
END COMPONENT; 

COMPONENT la_porta_or3
PORT(a,b,c: IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT la_porta_or4
PORT(a,b,c,d: IN BIT; z: OUT BIT);
END COMPONENT;  

COMPONENT la_porta_inv
PORT(a: IN BIT; z: OUT BIT);
END COMPONENT; 

-- Declarem les 4 senyals d'entrada i les 7 senyals de sortida (11 en total) 
SIGNAL ent1, ent2, ent3, ent4, sort_and2_logica, sort_and3_logica, sort_and4_logica,
	sort_or2_logica, sort_or3_logica, sort_or4_logica, sort_inv_logica: BIT;
 
-- Relacionem el component que volem testejar amb una entitat i arquitectura 
FOR DUT1: la_porta_and2 USE ENTITY WORK.and2(logica);
FOR DUT2: la_porta_and3 USE ENTITY WORK.and3(logica); 
FOR DUT3: la_porta_and4 USE ENTITY WORK.and4(logica);
FOR DUT4: la_porta_or2 USE ENTITY WORK.or2(logica);
FOR DUT5: la_porta_or3 USE ENTITY WORK.or3(logica);
FOR DUT6: la_porta_or4 USE ENTITY WORK.or4(logica);
FOR DUT7: la_porta_inv USE ENTITY WORK.inv(logica);
-- Hem acabat la declaraci� i ara comen�a el cos de l?arquitectura
BEGIN
-- Aqu� establim la relaci� entre els senyals abans definits i els
-- terminals del component a testejar. El programa els relaciona
-- en el mateix ordre que estan a l?entitat: el 1r senyal extern ser� el 1r
-- senyal de l?entitat
DUT1: la_porta_and2 PORT MAP (ent1, ent2, sort_and2_logica);
DUT2: la_porta_and3 PORT MAP (ent1, ent2, ent3, sort_and3_logica);
DUT3: la_porta_and4 PORT MAP (ent1, ent2, ent3, ent4, sort_and4_logica);
DUT4: la_porta_or2 PORT MAP (ent1, ent2, sort_or2_logica);
DUT5: la_porta_or3 PORT MAP (ent1, ent2, ent3, sort_or3_logica);
DUT6: la_porta_or4 PORT MAP (ent1, ent2, ent3, ent4, sort_or4_logica);
DUT7: la_porta_inv PORT MAP (ent1, sort_inv_logica);


-- Comen�a l?an�lisi de la variaci� de les variables, que cal especificar
-- expl�citament. Indiquem al PROCESS la variaci� de totes les variables
-- que volem que el programa analitzi, aqu� les d?entrada ent1 i ent2.
PROCESS (ent1,ent2)
BEGIN
ent1<=NOT ent1 AFTER 50 ns;
ent2<=NOT ent2 AFTER 100 ns;
ent3<=NOT ent3 after 150 ns; 
ent4<=NOT ent4 AFTER 200 ns;
END PROCESS;
END test;
